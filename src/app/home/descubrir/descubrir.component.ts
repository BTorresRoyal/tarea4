import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AmiiboModel } from '../amiibo.model';

@Component({
  selector: 'app-descubrir',
  templateUrl: './descubrir.component.html',
  styleUrls: ['./descubrir.component.scss'],
})
export class DescubrirComponent implements OnInit {
  

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {}

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }
}
