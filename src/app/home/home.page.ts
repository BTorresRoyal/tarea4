import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ActionSheetController,
  AlertController,
  LoadingController,
  ModalController,
} from '@ionic/angular';
import { DescubrirComponent } from './descubrir/descubrir.component';
import { AmiiboModel } from './amiibo.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  amiibo: AmiiboModel[] = [];
  isLoading = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController,
    private actionCtrl: ActionSheetController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController
  ) {}

  getAmiibo() {
    this.isLoading = true;
    this.amiibo = [];
    let random = () => Math.floor(Math.random() * 101);
    let getData = async () => {
      let url = 'https://amiiboapi.com/api/amiibo/';
      let res = await fetch(url);
      let data = await res.json();
      let arr = data.amiibo;

      for (let i = 0; i < 3; i++) {
        let id = random();
        let { amiiboSeries, character, gameSeries, image, name } = arr[id];
        this.amiibo[i] = {
          id,
          amiiboSeries,
          character,
          gameSeries,
          image,
          name,
        };
      }
      this.isLoading = false;
    };
    getData();
  }

  ngOnInit() {
    this.isLoading = true;
  }
  onDescubrir(id:number) {
    this.actionCtrl
      .create({
        header: 'Selecciona Accion',
        buttons: [
          {
            text: 'Seleccionar Amiibo' + id,
            handler: () => {
              //aqui usamos la funcion para abrir el modal
              this.openModal('select', id);
            },
          },
          { text: 'Cancelar', role: 'cancel' },
        ],
      })
      .then((actionSheet) => {
        actionSheet.present();
      });
  }

  openModal(modo: 'select', id:number) {
    this.modalCtrl
      .create({
        component: DescubrirComponent,
        componentProps: { amiibo: this.amiibo.find(el => el.id==id), mode: modo },
      })
      .then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      })
      .then((resultData) => {
        console.log(resultData);
      });
    console.log(modo);
  }
}
